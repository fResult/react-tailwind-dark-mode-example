import React, { useContext } from 'react';
import './App.css';
import { ThemeContext } from './ThemeProvider';

function App() {
  const { theme, setTheme } = useContext(ThemeContext)
  const isDark = theme === 'dark'

  return (
    <div className="bg-light-bg dark:bg-dark-bg text-light-text dark:text-dark-text h-screen flex flex-col justify-center items-center">
      <button
        className="block py-4 px-8 rounded-md bg-light-elem dark:bg-dark-elem shadow-xl focus:outline-none hover:border-green-900 hover:bg-blue-50 dark:hover:bg-gray-600"
        onClick={() => {
          setTheme && setTheme(isDark ? 'light' : 'dark')
        }}
      >
        {isDark ? 'Light' : 'Dark'}&nbsp;Mode
      </button>
      <div className="my-4 hello">
        <label className="mr-6">Your Name:</label>
        <input className="rounded-md h-14 w-96 p-4 bg-light-elem dark:bg-dark-elem shadow-xl focus:outline-none" />
      </div>
    </div>
  );
}

export default App;
