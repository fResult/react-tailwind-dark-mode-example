const tailwind = require('tailwindcss')
const postcss = require('autoprefixer')

module.exports = {
  style: {
    postcss: {
      plugins: [
        tailwind(),
        postcss()
      ]
    }
  }
}
