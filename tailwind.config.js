module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        dark: {
          elem: '#2B3945',
          bg: '#202C37',
          text: '#FFFFFF'
        },
        light: {
          elem: '#FFFFFF',
          bg: '#FAFAFA',
          text: '#111517'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
